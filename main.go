package main

import (
	"context"
	"errors"
	"log"
	"time"
)

/*
Задание: Шаблон "Таймаут"
В нашем приложении имеется функция, которая может выполняться гораздо дольше, чем мы себе можем позволить.
Функция находится в сторонней библиотеке и мы не можем ее исправить. Библиотека очень старая и функция еще не поддерживает context.
Необходимо каким-то образом декорировать эту функцию в нашем приложении, чтобы она поддерживала context с отменой по таймауту.

Примечания:

обратите внимание на ctx.WithTimeout и ctx.Done
или можно воспользоваться каналом, который возврашает функция time.After
*/

func main() {

	ctx := context.Background()
	// создаем дочерний контекст, который можно распостранить в нижележащине функции
	timeLimit := 1000 //time.Millisecond
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// запускаем горутину, которая отменит наш контекст

	// запускаем оболочку к функции LegacyFunction и передаем контекст
	a := 10
	res, err := LegacyFunctionWrapper(ctx, a, time.Duration(timeLimit))
	log.Println(res, err)
	log.Println("Exit")

}

func LegacyFunctionWrapper(ctx context.Context, a int, timeLimit time.Duration) (int, error) {

	c := make(chan int)
	var res int
	var err error

	go func() {
		log.Printf("Start LegacyFunction %v", time.Now().Format(time.RFC3339))
		res, err = LegacyFunction(a)
		log.Printf("Finish LegacyFunction %v", time.Now().Format(time.RFC3339))
		c <- 1
	}()

	for {
		select {
		case <-c:
			{
				return res, err
			}
		case t := <-time.After(timeLimit * time.Millisecond):
			log.Printf("Wrapper timeout (%v Milliseconds): %v", int(timeLimit), t.Format(time.RFC3339))
			return 0, errors.New("Timeout")
		case <-ctx.Done():
			return 0, ctx.Err()
		}
	}
}

func LegacyFunction(a int) (int, error) {

	// какая-то крайне полезная работа
	b := a * 10
	if b >= 1000000 {
		return 0, errors.New("error b > 1000")
	}

	time.Sleep(time.Duration(b * int(time.Millisecond)))

	return b, nil
}
